// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CIrrDeviceStub.h"
#include "CSceneManager.h"

namespace saga
{
//! constructor
CIrrDeviceStub::CIrrDeviceStub(const SIrrlichtCreationParameters& params)
:  SagaDevice(), // VideoDriver(0), // SceneManager(0),
   CreationParams(params), Close(false)
{

}

CIrrDeviceStub::~CIrrDeviceStub()
{

}

//! add event listener
void CIrrDeviceStub::addEventReceiver(IEventReceiver* receiver)
{
  if (receiver) EventReceivers.push_back(receiver);
}

// ! creates scene manager
void CIrrDeviceStub::createSceneManager()
{
  SceneManager = std::make_unique<scene::CSceneManager>(VideoDriver);
}

//! Checks if the window is running in fullscreen mode
bool CIrrDeviceStub::isFullscreen() const
{
  return CreationParams.Fullscreen;
}

} // namespace saga

