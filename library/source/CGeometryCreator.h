// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_GEOMETRY_CREATOR_H_INCLUDED__
#define __C_GEOMETRY_CREATOR_H_INCLUDED__

#include "IGeometryCreator.h"
#include "SMeshBuffer.h"

namespace saga
{

namespace scene
{

//! class for creating geometry on the fly
class CGeometryCreator : public IGeometryCreator
{
  void addToBuffer(const video::S3DVertex& v, SMeshBuffer* Buffer) const;
public:
  IMesh* createCubeMesh(const glm::vec3& size) const;

  IMesh* createHillPlaneMesh(
    const glm::vec2& tileSize, const glm::uvec2& tileCount,
    video::SMaterial* material, float hillHeight, const glm::vec2& countHills,
    const glm::vec2& textureRepeatCount) const;

  IMesh* createGeoplaneMesh(float radius, std::uint32_t rows, std::uint32_t columns) const;

  IMesh* createTerrainMesh(video::IImage* texture,
    video::IImage* heightmap, const glm::vec2& stretchSize,
    float maxHeight, video::IVideoDriver* driver,
    const glm::uvec2& defaultVertexBlockSize,
    bool debugBorders=false) const;

  IMesh* createArrowMesh(const std::uint32_t tesselationCylinder,
      const std::uint32_t tesselationCone, const float height,
      const float cylinderHeight, const float width0,
      const float width1, const video::SColor vtxColor0,
      const video::SColor vtxColor1) const;

  IMesh* createSphereMesh(float radius, std::uint32_t polyCountX, std::uint32_t polyCountY) const;

  IMesh* createCylinderMesh(float radius, float length, std::uint32_t tesselation,
        const video::SColor& color= 0xffffffff,
        bool closeTop=true, float oblique= 0.f) const;

  IMesh* createConeMesh(float radius, float length, std::uint32_t tesselation,
        const video::SColor& colorTop= 0xffffffff,
        const video::SColor& colorBottom= 0xffffffff,
        float oblique= 0.f) const;

  IMesh* createVolumeLightMesh(
      const std::uint32_t subdivideU=32, const std::uint32_t subdivideV=32,
      const video::SColor footColor= 0xffffffff,
      const video::SColor tailColor= 0xffffffff,
      const float lpDistance = 8.f,
      const glm::vec3& lightDim = glm::vec3(1.f,1.2f,1.f)) const;
};


} // namespace scene
} // namespace saga

#endif

