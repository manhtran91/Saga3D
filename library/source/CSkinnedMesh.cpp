#include "CSkinnedMesh.h"
#include "IAnimatedMeshSceneNode.h"
#include <glm/gtc/type_ptr.hpp>

namespace saga
{
namespace scene
{

CSkinnedMesh::CSkinnedMesh()
{
  Mesh = std::make_shared<SMesh>();
}

CSkinnedMesh::~CSkinnedMesh()
{

}

void CSkinnedMesh::loadBones(const aiMesh* pMesh, uint32_t vertexOffset)
{
  for (uint32_t i = 0; i < pMesh->mNumBones && i < MAX_BONES; i++)
  {
    uint32_t index = 0;

    std::string name(pMesh->mBones[i]->mName.data);

    if (BoneMapping.find(name) == BoneMapping.end())
    {
      index = BoneCount;
      BoneCount++;
      SBoneInfo bone;
      BoneInfo.push_back(bone);
      BoneInfo[index].Offset = pMesh->mBones[i]->mOffsetMatrix;
      BoneMapping[name] = index;
    }
    else
    {
      index = BoneMapping[name];
    }

    for (uint32_t j = 0; j < pMesh->mBones[i]->mNumWeights; j++)
    {
      uint32_t vertexID = vertexOffset + pMesh->mBones[i]->mWeights[j].mVertexId;
      Bones[vertexID].add(index, pMesh->mBones[i]->mWeights[j].mWeight);
    }
  }
  BoneTransforms.resize(BoneCount);
}

glm::mat4 CSkinnedMesh::getBoneTransform(const std::uint32_t boneID) const
{
  return glm::transpose(glm::make_mat4(&BoneTransforms[boneID].a1));
}

void CSkinnedMesh::onAnimate(const float time)
{
  float TicksPerSecond = (float)(Scene->mAnimations[0]->mTicksPerSecond != 0 ? Scene->mAnimations[0]->mTicksPerSecond : 25.0f);
  float TimeInTicks = time * TicksPerSecond;
  float AnimationTime = fmod(TimeInTicks, (float)Scene->mAnimations[0]->mDuration);

  aiMatrix4x4 identity = aiMatrix4x4();
  readNodeHierarchy(AnimationTime, Scene->mRootNode, identity);

  for (uint32_t i = 0; i < BoneTransforms.size(); i++)
  {
    BoneTransforms[i] = BoneInfo[i].FinalTransformation;
  }
}

const aiNodeAnim* CSkinnedMesh::findNodeAnim(const aiAnimation* animation, const std::string nodeName)
{
  for (uint32_t i = 0; i < animation->mNumChannels; i++)
  {
    const aiNodeAnim* nodeAnim = animation->mChannels[i];
    if (std::string(nodeAnim->mNodeName.data) == nodeName)
    {
      return nodeAnim;
    }
  }
  return nullptr;
}

aiMatrix4x4 CSkinnedMesh::interpolateTranslation(float time, const aiNodeAnim* pNodeAnim)
{
  aiVector3D translation;

  if (pNodeAnim->mNumPositionKeys == 1)
  {
    translation = pNodeAnim->mPositionKeys[0].mValue;
  }
  else
  {
    uint32_t frameIndex = 0;
    for (uint32_t i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++)
    {
      if (time < (float)pNodeAnim->mPositionKeys[i + 1].mTime)
      {
        frameIndex = i;
        break;
      }
    }

    aiVectorKey currentFrame = pNodeAnim->mPositionKeys[frameIndex];
    aiVectorKey nextFrame = pNodeAnim->mPositionKeys[(frameIndex + 1) % pNodeAnim->mNumPositionKeys];

    float delta = (time - (float)currentFrame.mTime) / (float)(nextFrame.mTime - currentFrame.mTime);

    const aiVector3D& start = currentFrame.mValue;
    const aiVector3D& end = nextFrame.mValue;

    translation = (start + delta * (end - start));
  }

  aiMatrix4x4 mat;
  aiMatrix4x4::Translation(translation, mat);
  return mat;
}

aiMatrix4x4 CSkinnedMesh::interpolateRotation(float time, const aiNodeAnim* pNodeAnim)
{
  aiQuaternion rotation;

  if (pNodeAnim->mNumRotationKeys == 1)
  {
    rotation = pNodeAnim->mRotationKeys[0].mValue;
  }
  else
  {
    uint32_t frameIndex = 0;
    for (uint32_t i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++)
    {
      if (time < (float)pNodeAnim->mRotationKeys[i + 1].mTime)
      {
        frameIndex = i;
        break;
      }
    }

    aiQuatKey currentFrame = pNodeAnim->mRotationKeys[frameIndex];
    aiQuatKey nextFrame = pNodeAnim->mRotationKeys[(frameIndex + 1) % pNodeAnim->mNumRotationKeys];

    float delta = (time - (float)currentFrame.mTime) / (float)(nextFrame.mTime - currentFrame.mTime);

    const aiQuaternion& start = currentFrame.mValue;
    const aiQuaternion& end = nextFrame.mValue;

    aiQuaternion::Interpolate(rotation, start, end, delta);
    rotation.Normalize();
  }

  aiMatrix4x4 mat(rotation.GetMatrix());
  return mat;
}

aiMatrix4x4 CSkinnedMesh::interpolateScale(float time, const aiNodeAnim* pNodeAnim)
{
  aiVector3D scale;

  if (pNodeAnim->mNumScalingKeys == 1)
  {
    scale = pNodeAnim->mScalingKeys[0].mValue;
  }
  else
  {
    uint32_t frameIndex = 0;
    for (uint32_t i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++)
    {
      if (time < (float)pNodeAnim->mScalingKeys[i + 1].mTime)
      {
        frameIndex = i;
        break;
      }
    }

    aiVectorKey currentFrame = pNodeAnim->mScalingKeys[frameIndex];
    aiVectorKey nextFrame = pNodeAnim->mScalingKeys[(frameIndex + 1) % pNodeAnim->mNumScalingKeys];

    float delta = (time - (float)currentFrame.mTime) / (float)(nextFrame.mTime - currentFrame.mTime);

    const aiVector3D& start = currentFrame.mValue;
    const aiVector3D& end = nextFrame.mValue;

    scale = (start + delta * (end - start));
  }

  aiMatrix4x4 mat;
  aiMatrix4x4::Scaling(scale, mat);
  return mat;
}

void CSkinnedMesh::readNodeHierarchy(float AnimationTime, const aiNode* pNode, const aiMatrix4x4& ParentTransform)
{
  std::string NodeName(pNode->mName.data);

  aiMatrix4x4 NodeTransformation(pNode->mTransformation);

  const aiNodeAnim* pNodeAnim = findNodeAnim(Animation, NodeName);

  if (pNodeAnim)
  {
    // Get interpolated matrices between current and next frame
    aiMatrix4x4 matScale = interpolateScale(AnimationTime, pNodeAnim);
    aiMatrix4x4 matRotation = interpolateRotation(AnimationTime, pNodeAnim);
    aiMatrix4x4 matTranslation = interpolateTranslation(AnimationTime, pNodeAnim);

    NodeTransformation = matTranslation * matRotation * matScale;
  }

  aiMatrix4x4 GlobalTransformation = ParentTransform * NodeTransformation;

  if (BoneMapping.find(NodeName) != BoneMapping.end())
  {
    uint32_t BoneIndex = BoneMapping[NodeName];
    BoneInfo[BoneIndex].FinalTransformation = GlobalInverseTransform * GlobalTransformation * BoneInfo[BoneIndex].Offset;
  }

  for (uint32_t i = 0; i < pNode->mNumChildren; i++)
  {
    readNodeHierarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
  }
}

} // namespace scene
} // namespace saga
