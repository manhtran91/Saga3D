// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CSceneNodeAnimatorCameraFPS.h"
#include "IVideoDriver.h"
#include "ISceneManager.h"
#include "ICameraSceneNode.h"

namespace saga
{
namespace scene
{

//! constructor
CSceneNodeAnimatorCameraFPS::CSceneNodeAnimatorCameraFPS(
  float moveSpeed, float rotateSpeed)
  : MoveSpeed(moveSpeed), RotateSpeed(rotateSpeed)
{
  
}

//! destructor
CSceneNodeAnimatorCameraFPS::~CSceneNodeAnimatorCameraFPS()
{

}

//! It is possible to send mouse and key events to the camera. Most cameras
//! may ignore this input, but camera scene nodes which are created for
//! example with scene::ISceneManager::addMayaCameraSceneNode or
//! scene::ISceneManager::addFPSCameraSceneNode, may want to get this input
//! for changing their position, look at target or whatever.
void CSceneNodeAnimatorCameraFPS::onEvent(const SDL_Event& event)
{
  switch(event.type)
  {
    case SDL_KEYDOWN:
    {
      switch (event.key.keysym.scancode)
      {
        case SDL_SCANCODE_W:
        {
          MoveDirection.y = 1.0;
        } break;
        case SDL_SCANCODE_S:
        {
          MoveDirection.y = -1.0;
        } break;
        case SDL_SCANCODE_A:
        {
          MoveDirection.x = -1.0;
        } break;
        case SDL_SCANCODE_D:
        {
          MoveDirection.x = 1.0;
        } break;
      } break;
    }

    case SDL_KEYUP:
    {
      switch (event.key.keysym.scancode)
      {
        case SDL_SCANCODE_W:
        case SDL_SCANCODE_S:
        {
          MoveDirection.y = 0.0;
        } break;

        case SDL_SCANCODE_A:
        case SDL_SCANCODE_D: {
          MoveDirection.x = 0.0;
        } break;

      } break;
    }

    case SDL_MOUSEMOTION:
    {
      glm::ivec2 mouse(event.motion.xrel, event.motion.yrel);
      mouse *= RotateSpeed;
      Rotation += glm::vec3(mouse.y, -mouse.x, 0);
    } break;

    default:
      break;
  }
}

void CSceneNodeAnimatorCameraFPS::animateNode(ISceneNode& node, const float time)
{
  if (node.getType() != E_SCENE_NODE_TYPE::CAMERA)
    return;

  ICameraSceneNode& camera = *static_cast<ICameraSceneNode*>(&node);
  auto& smgr = camera.getSceneManager();

  if (smgr == nullptr)
    return;
  
  DeltaTime = (float)(time - LastAnimationTime) * 10;
  LastAnimationTime = time;

  glm::vec3 up = {0.0f, 0.0f, -1.0f};
  Rotation.x = glm::clamp(Rotation.x, -89.0f, 89.0f); //limit pitch to prevent glitches
  glm::vec2 radians = { glm::radians(Rotation.x), glm::radians(Rotation.y) }; //x-pitch y-yaw

  float posX = 100 * (std::cos(radians.x) * std::sin(radians.y));
  float posY = 100 * (std::cos(radians.x) * std::cos(radians.y));
  float posZ = 100 * (std::sin(radians.x));
  
  glm::vec3 dir(posX, posY, posZ);
  glm::vec3 forwardDir = glm::normalize(dir);
  glm::vec3 sideDir = glm::cross(forwardDir, up);

  //Move forward or sideways if keys are pressed 
  Position += (forwardDir * (MoveDirection.y * MoveSpeed * DeltaTime));
  Position += (sideDir * (MoveDirection.x * MoveSpeed * DeltaTime));
  
  //set positions
  camera.setTarget(Position + dir);
  camera.setPosition(Position);
  camera.setUpVector(up);
}

//! Sets the rotation speed
void CSceneNodeAnimatorCameraFPS::setRotateSpeed(float speed)
{
  RotateSpeed = speed;
}

//! Sets the movement speed
void CSceneNodeAnimatorCameraFPS::setMoveSpeed(float speed)
{
  MoveSpeed = speed;
}

//! Gets the rotation speed
float CSceneNodeAnimatorCameraFPS::getRotateSpeed() const
{
  return RotateSpeed;
}

// Gets the movement speed
float CSceneNodeAnimatorCameraFPS::getMoveSpeed() const
{
  return MoveSpeed;
}

//! Sets whether the Y axis of the mouse should be inverted.
void CSceneNodeAnimatorCameraFPS::setInvertMouse(bool invert)
{

}

} // namespace scene
} // namespace saga
