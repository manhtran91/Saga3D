#ifndef __E_BLEND_OP_H_INCLUDED__
#define __E_BLEND_OP_H_INCLUDED__

namespace saga
{
namespace video
{

enum E_BLEND_OPERATION
{
  NONE = 0,	//!< No blending happens
  ADD,		//!< Default blending adds the color values
  SUBTRACT,	//!< This mode subtracts the color values
  REVSUBTRACT,//!< This modes subtracts destination from source
  MIN,		//!< Choose minimum value of each color channel
  MAX,		//!< Choose maximum value of each color channel
  MIN_FACTOR,	//!< Choose minimum value of each color channel after applying blend factors, not widely supported
  MAX_FACTOR,	//!< Choose maximum value of each color channel after applying blend factors, not widely supported
  MIN_ALPHA,	//!< Choose minimum value of each color channel based on alpha value, not widely supported
  MAX_ALPHA	//!< Choose maximum value of each color channel based on alpha value, not widely supported
};

} // namespace scene
} // namespace saga

#endif

