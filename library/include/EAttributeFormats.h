#ifndef __E_ATTRIBUTE_FORMATS_H_INCLUDED__
#define __E_ATTRIBUTE_FORMATS_H_INCLUDED__
#include <cstdint>

namespace saga
{
namespace video
{

enum class E_ATTRIBUTE_FORMAT {
  FLOAT,
  FLOAT2,
  FLOAT3,
  FLOAT4,
  BYTE4,
  BYTE4N,
  UBYTE4,
  UBYTE4N,
  SHORT2,
  SHORT2N,
  SHORT4,
  SHORT4N,
  UINT10_N2
};

uint32_t GetAttributeSize(const E_ATTRIBUTE_FORMAT format);
uint32_t GetAttributeComponentSize(const E_ATTRIBUTE_FORMAT format);

} // namespace video
} // namespace saga

#endif // __E_ATTRIBUTE_FORMATS_H_INCLUDED__

