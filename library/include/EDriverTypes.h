// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __E_DRIVER_TYPES_H_INCLUDED__
#define __E_DRIVER_TYPES_H_INCLUDED__

namespace saga
{
namespace video
{
  //! An enum class for all types of drivers the Irrlicht Engine supports.
  enum class E_DRIVER_TYPE
  {
    //! NULL driver,
    /** The null device is able to load texture but doesn't render or display any graphics. */
    NULL_DRIVER,

    //! Vulkan NULL driver, useful for applications to run the engine without visualization.
    /** The null device is able to render to images or compute but doesn't display any graphics. */
    VULKAN_NULL,

    //! Vulkan device, available on most platforms.
    /** Performs hardware accelerated rendering of 3D and 2D
    primitives. */
    VULKAN,

    //! No driver, just for counting the elements
    COUNT
  };

  const char* const DRIVER_TYPE_NAMES[] =
  {
    "NullDriver",
    "VulkanNullDriver",
    "VulkanDriver",
    nullptr
  };

  const char* const DRIVER_TYPE_NAMES_SHORT[] =
  {
    "null",
    "vknull",
    "vk",
    nullptr
  };

} // namespace video
} // namespace saga


#endif
