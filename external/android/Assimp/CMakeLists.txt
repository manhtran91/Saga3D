add_library(Dependencies::Assimp SHARED IMPORTED GLOBAL)
target_include_directories(Dependencies::Assimp
  INTERFACE ${CMAKE_SOURCE_DIR}/android/app/imported-libs/include/
)
