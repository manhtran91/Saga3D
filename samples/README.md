# Saga3D Samples

## [01 Window](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/01_window)
Create a window and clear it to a color.

![](https://i.imgur.com/hfSHc5F.png)

## [02 Triangle](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/02_triangle)
Draw a triangle and color it using fragment shader.

Also demonstrate custom vertex attribute and push_constant.

![](https://i.imgur.com/Cid3wfV.png)

## [03 Mesh](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/03_mesh)
Load a mesh and do texture mapping via shader.

Use your mouse to look around and WASD keys to move camera.

![](https://i.imgur.com/vXyxfgu.png)

## [04 Multiple render targets](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/04_multiple_render_targets)
Render scene to multiple textures.

Then copy 3 textures to one texture and display.

Because Vulkan's swapchain format is different, we use blitTexture to convert color format.

Texture contents:
- Left: position texture
- Middle: albedo texture
- Right: depth texture (why this looks abnormal? depth is a single value while pixel has 4 channel values)

![](https://i.imgur.com/jQz0gkH.jpg)

## [05 GPU skinning animation](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/05_animation)

Animated model with GPU skinning. Model file is the wolf from [Free3D](https://free3d.com/3d-model/wolf-rigged-and-game-ready-42808.html)

![](https://media.giphy.com/media/1wmOVY5NwOqsXAeMZB/giphy.gif)
